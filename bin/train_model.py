import numpy as np
import matplotlib.pyplot as plt
from gwpy.timeseries import TimeSeries
from corner import corner
from tqdm import tqdm
from scipy.signal import tukey
from scipy.stats.distributions import norm
from mpl_toolkits.mplot3d import Axes3D

from argparse import ArgumentParser

ARGS = []

def setup_parser():
    parser = ArgumentParser()
    parser.add_argument("train-file", type=str, help="training set of glitches")
    #parser.add_argument("--test-file", type=str, help="optionally run on a test set")
    parser.add_argument("--window", type=float, default=0.5, help="training window length in seconds")
    parser.add_argument("--q", type=int, required=True, help="# of PPCA components")
    parser.add_argument("--det", type=str, help="detector")
    parser.add_argument("--epoch", type=str, help="epoch")
    parser.add_argument("--srate", type=int, default=2048, help="sample rate of training and run data (expects the same)")
    parser.add_argument("--glitchtype", type=str, default = "Tomte", help="glitchtype by gravityspy name")
    parser.add_argument("--outpref", type=str, default="ppcamodel_trained", help="file prefix for output")
    parser.add_argument("--plots", type=bool, default=False, help="whether to plot training results")
    #parser.add_argument("--test", type=bool, default=False, help="whether to run diagnostics on a test set")
    return vars(parser.parse_args())

def main():
    input_args = setup_parser() # get input args
    model_args = {}

    glitches = np.load(input_args["train-file"])
#    if input_args["test"] == True:
#       test = np.load(input_args["test-file"])
    srate = input_args["srate"]

    outtag = "{}_{}_{}_{}_q{}_{}Hz".format(input_args["outpref"],
                                           input_args["det"],
                                           input_args["epoch"],
                                           input_args["glitchtype"],
                                           input_args["q"],
                                           input_args["srate"])

    #prepare data

    times = np.arange(glitches.shape[1]) / srate
    siglen = 1.

    train_size = glitches.shape[0]
    timeseries_data = []

    white_timeseries_data = np.empty((train_size, int(srate * siglen)))
    white_freqseries_data = np.empty((train_size, int((srate * siglen) / 2 + 1)), dtype='complex')

    for i, raw_ts in enumerate(glitches):
        ts = TimeSeries(raw_ts, times=times)
        white_timeseries_data[i] = ts.value
        white_freqseries_data[i] = ts.fft().value
        timeseries_data.append(ts)



    # if input_args["test"] == True:
    #     test_size = test.shape[0]
    #     testtimeseries_data = []
    #
    #     testwhite_timeseries_data = np.empty((test_size, int(srate * siglen)))
    #     testwhite_freqseries_data = np.empty((test_size, int((srate * siglen) / 2 + 1)), dtype='complex')
    #
    #     for i, raw_ts in enumerate(test):
    #         ts = TimeSeries(raw_ts, times=times)
    #         testwhite_timeseries_data[i] = ts.value
    #         testwhite_freqseries_data[i] = ts.fft().value
    #         testtimeseries_data.append(ts)
    #
    #     testX_fd = np.fft.rfft(window * testwhite_timeseries_data[:, sig_start_idx:sig_end_idx])

    sigcentidx = int(glitches.shape[1]/2)  # where to center our glitches. assumes centered
    #found a potential error in sigcentidx
    idx_win = int(input_args["window"]*input_args["srate"])  # segment length we want to train with in samples
    window = tukey(idx_win, .2)
    sig_start_idx = sigcentidx - idx_win // 2
    sig_end_idx = sigcentidx + idx_win // 2


    X_fd = np.fft.rfft(window * white_timeseries_data[:, sig_start_idx:sig_end_idx])
    X = np.concatenate([X_fd.real, X_fd.imag], axis=1).T  # packed F domain

    mu = X.mean(axis=1)  # don't subtract the mean, previously X -= mu[:, np.newaxis]
    noise_std = 1 / 2 / (idx_win / srate)  # was 2/(idx_win/srate), led to \simga = 2.0


    #testX = np.concatenate([testX_fd.real, testX_fd.imag], axis=1).T

    #train the model

    num_datapoints = X.shape[1]  # = N
    data_dim = X.shape[0]  # =d
    q = input_args["q"]  # latent_dim
    stddv_datapoints = noise_std

    S = np.cov(X)  # sample covariance of observations
    λ, U = np.linalg.eigh(S)  # eigenvalue decomposition of S

    λ = (λ[::-1])  # eigenvalues
    U = (U[:, ::-1])  # eigenvectors (we get them all for free)
    λs = λ[:q]  # truncate
    Uq = U[:, :q]
    Λq = np.diagflat(λs)  # qxq

    # W is dxq
    W = np.matmul(Uq, np.sqrt(Λq - np.diagflat(noise_std ** 2 * np.ones(q))))  # let R=I

    C = np.matmul(W, W.T) + noise_std ** 2 * np.identity(W.shape[0])  # covariance
    M = np.matmul(W.T, W) + noise_std ** 2 * np.identity(W.shape[1])

    # reconstruction steps
    Minv = np.linalg.pinv(M)  # pseudoinverse, since w is not square.
    Ztrain = np.matmul(Minv, np.dot(W.T, X))  # - mu[:, np.newaxis]))

    # if input_args["test"]==True:
    #     Ztest = np.matmul(Minv, np.dot(W.T, testX)) #- mu[:, np.newaxis]))
    #     rec = np.dot(W, Ztest)  # all test set reconstructions, in packed F-domain

    w_fd_re, w_fd_im = np.split(W, 2, axis=0)
    w_fd = w_fd_re + 1j * w_fd_im
    Wtd = np.fft.irfft(w_fd, axis=0)

    np.save("./result/models/W_{}.npy".format(outtag), Wtd) #save the time domain training weights.

    if input_args["plots"]==True:
        plt.style.use(['dark_background'])
        plt.figure(figsize=(12, 8))
        plt.plot(Wtd)
        plt.xlabel("")
        plt.savefig('./result/plots/training/{}_eigenfuncs_TD.png'.format(outtag))

        fig = corner(Ztrain.T, color='orange');
        fig.savefig('./result/plots/training/{}_corner.png'.format(outtag))

        fig = plt.figure(1, figsize=(10, 10))
        ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
        ax.scatter(Ztrain.T[:, 0], Ztrain.T[:, 1], Ztrain.T[:, 2], cmap=plt.cm.nipy_spectral,
                   edgecolor='k', label='training data', alpha=0.3)
        ax.legend()
        fig.savefig('./result/plots/training/{}_3spaceweights.png'.format(outtag))

    ## add residuals plotting function...

if __name__ == "__main__":
    main()