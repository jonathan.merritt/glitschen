import numpy as np
import matplotlib.pyplot as plt
from gwpy.timeseries import TimeSeries
from corner import corner
from tqdm import tqdm
from scipy.signal import tukey
from scipy.stats.distributions import norm
import kombine
import emcee
import datetime
from argparse import ArgumentParser
import os
os.environ["OMP_NUM_THREADS"] = "1"
import pickle

from glitschen.utils import *
from glitschen.sample import *

def setup_parser():
    parser = ArgumentParser()
    parser.add_argument("runfile", type=str, help="run file")
    parser.add_argument("--W", type=str, help="trained TD weights")
    parser.add_argument("--srate", type=float, default=2048, help="sample rate of training/test data (expects same)")
    parser.add_argument("--prior", type=str, default = "kde", help="kde or gaussian prior")
    parser.add_argument("--gprwidth", type=float, default = 1.0, help="gaussian prior width")
    parser.add_argument("--nwalkers", type=int, default=12, help="number of walkers initialized")
    parser.add_argument("--nsteps", type=int, default=1000, help="numper of MCMC steps post burnin")
    parser.add_argument("--gps", type=float, default=0.0, help="gps time at center of runfile")
    parser.add_argument("--crop", type=bool, default = False, help="whether to crop the test segment")
    parser.add_argument("--runwindow", type=float, default = 2.0, help="crop length with event time centered")
    parser.add_argument("--center", type=float, default = None, help="event time in segment(from zero)")
    parser.add_argument("--outtag", type=str, default=None, help="name for run")
    return vars(parser.parse_args())

def main():
    input_args = setup_parser() # get input args
    model_args = {}

    W = np.load(input_args["W"])
    q = W.shape[1] #read # components from W
    runwindow = input_args["runwindow"]
    test = np.load(input_args["runfile"]) #time domain data
    srate = input_args["srate"]
    cent = input_args["center"]
    seglen = len(test)/srate
    gps = input_args["gps"]
    nwalkers = input_args["nwalkers"]
    N = input_args["nsteps"]
    outtag = input_args["outtag"]
    if outtag==None:
        outtag = input_args["W"][2:]

    priortag = input_args["prior"]
    if priortag == "kde":
        prior = kombine.clustered_kde.optimized_kde(W)
    else:
        gaussian_prior_width = input_args["gprwidth"]
        n = norm(loc=0, scale=gaussian_prior_width)
        prior = lambda x: sum([n.logpdf(p) for p in x])


    if cent == None:
        cent = seglen/2

    ts = TimeSeries(test, times=np.linspace(0, seglen, int((seglen*srate)+1)))
    if input_args["crop"] == True:
        tsc = ts.crop(cent-runwindow/2, cent+runwindow/2)
    else:
        tsc = ts
    tsc.epoch = gps-runwindow/2
    test = tsc.value

    window = window = tukey(len(test), .2)
    test_fd = np.fft.rfft(test * window)

    post = Posterior(test_fd, W, srate, prior)

    sampling_start = datetime.datetime.now()

    if W.shape[0] < len(test): #time translation active
        randparams = np.random.normal(loc=0, scale=1, size=(nwalkers, q))
        randtime = np.random.normal(loc=0.0, scale=0.2, size=(nwalkers, 1))
        randparams = np.append(randtime, randparams, axis=1)

        sampler = kombine.Sampler(nwalkers=nwalkers, ndim=q+1, lnpostfn=post)
        sampler.burnin(p0=randparams, verbose=True)
        print("burnin completed taking time: {}".format(datetime.datetime.now() - sampling_start))
        sampler.run_mcmc(N=N, verbose=True)
        print("sampling finished taking time: {}".format(datetime.datetime.now() - sampling_start))
        print("generating reconstructions...")

        samps = sampler.get_samples() #independent samples, thinned based on acceptance rates.

        rec = np.dot(W, samps[:, 1:].T) #wtd
        reconst = rec.T
        reconst_td = np.zeros(shape=(samps.shape[0], len(test))) #initialze padding array
        t_shifts = samps[:, 0]

        for i in range(0, len(samps)):  # pad and align reconstructions. fd method
            reconst_td[i] = np.pad(reconst[i], (post.zeros_before, post.zeros_after))
            reconst_fd = np.fft.rfft(reconst_td[i])
            reconst_fd_shifted = fd_timeshift(reconst_fd, post.tfreqs, t_shifts[i], post.srate)
            reconst_td[i] = np.fft.irfft(reconst_fd_shifted)

        ##plots
        plt.style.use('dark_background')

        logls = np.zeros(shape=samps.shape[0])
        for i in range(0, len(samps)):
            logls[i] = post.logl(reconstruction=post.reconstruct(params=samps[i]))
        SNRs = np.zeros(shape=samps.shape[0])
        for i in range(0, len(samps)):
            SNRs[i] = post.matched_filter_snr(samps[i])

        plt.figure() #logls
        plt.hist(logls, bins=50);
        plt.savefig("./result/plots/runs/{}_logls.png".format(outtag))

        plt.figure() #snrs
        plt.hist(SNRs, bins=30);
        plt.savefig("./result/plots/runs/{}_SNRs.png".format(outtag))

        plt.figure(figsize=(14, 8)) #recons
        plt.title('PPCA reconstruction with time translation')
        plt.xlabel('time samples')
        plt.ylabel('whitened amplitude')
        plt.plot(test, alpha=1.0, label='data')
        plt.plot(reconst_td.T, alpha=0.02, color='orange', label='all samples');
        plt.plot(reconst_td[np.argmax(logls)], color='red', label='max L reconstruction')
        #plt.legend()
        plt.savefig("./result/plots/runs/{}_timeseries_reconstructions.png".format(outtag))

        plt.figure(figsize=(14, 3)) #time samples
        plt.title('Center Time, all samples')
        plt.hist(samps[:, 0], bins=600);
        plt.yscale('log')
        plt.ylabel('log10 samples')
        plt.xlabel('time shifted from center, seconds')
        plt.savefig("./result/plots/runs/{}_center_time_distribution.png".format(outtag))

        fig = corner(samps[:], labels=['$t_{center}$', '$q_1$', '$q_2$', '$q_3$', '$q_4$', '$q_5$'],
                     color='orange')
        fig.savefig("./result/plots/runs/{}_cornerWtime.png".format(outtag))

    else: # fixed time
        randparams = np.random.normal(loc=0, scale=1, size=(nwalkers, q))

        sampler = kombine.Sampler(nwalkers=nwalkers, ndim=q, lnpostfn=post)
        sampler.burnin(p0=randparams, verbose=True)
        sampler.run_mcmc(N=N, verbose=True)

        samps = sampler.get_samples() #independent samples, thinned based on acceptance rates.

        rec = np.dot(W, samps.T) #wtd
        reconst_td = rec.T

        print("sampling finished taking time: {}".format(datetime.datetime.now() - sampling_start))
        print("generating reconstructions...")

        ##plots
        plt.style.use('dark_background')

        logls = np.zeros(shape=samps.shape[0])
        for i in range(0, len(samps)):
            logls[i] = post.logl(reconstruction=post.reconstruct(params=samps[i]))
        SNRs = np.zeros(shape=samps.shape[0])
        for i in range(0, len(samps)):
            SNRs[i] = post.matched_filter_snr(samps[i])

        plt.figure() #logls
        plt.hist(logls, bins=50);
        plt.savefig("./result/plots/runs/{}_logls.png".format(outtag))

        plt.figure() #snrs
        plt.hist(SNRs, bins=30);
        plt.savefig("./result/plots/runs/{}_SNRs.png".format(outtag))

        plt.figure(figsize=(14, 8)) #recons
        plt.title('PPCA reconstruction with time translation')
        plt.xlabel('time samples')
        plt.ylabel('whitened amplitude')
        plt.plot(test, alpha=1.0, label='data')
        plt.plot(reconst_td.T, alpha=0.02, color='orange', label='all samples');
        plt.savefig("./result/plots/runs/{}_timeseries_reconstructions.png".format(outtag))

        fig = corner(samps[:], labels=['$q_1$', '$q_2$', '$q_3$', '$q_4$', '$q_5$'],
                     color='orange')
        fig.savefig("./result/plots/runs/{}_corner.png".format(outtag))

    np.save("./result/samples/{}_independent_samples_asarray.npy".format(outtag), samps)
    save_result_to_file(sampler, "./result/samples/{}_sampler.pkl".format(outtag))



if __name__ == "__main__":
    main()
