#!/home/j/PycharmProjects/glitschenpy39test/bin/python
"""
This file is to serve as a script to preprocess a
    .npy file with glitch data in each row of the matrix
"""

from argparse import ArgumentParser
from glitschen.preprocess import preprocessdata


def setup_parser():
    parser = ArgumentParser()
    parser.add_argument("--raw-file", type=str, required=True, help=".npy file with unprocessed glitches as each "
                                                                    "row in the matrix")
    parser.add_argument("--outfile", type=str, required=True, help="path of where to save the processed .npy file to")
    parser.add_argument("--split-test", default=False, action='store_true', help="boolean flag to separate 10% of "
                                                                                 "glitches for testing")
    parser.add_argument("--srate", type=float, default=16384., help="sample rate of the data in raw file")
    parser.add_argument("--lowpass", default=None, type=float, help="freq to lowpass from (default to no lowpassing)")
    parser.add_argument("--highpass", default=None, type=float, help="freq to highpass from (default to no highpassing")
    parser.add_argument("--glitchseglen", type=float, default=0.4, help="segment length of glitches to save (seconds)")
    parser.add_argument("--glitch-center", type=float, default=None, help='Manually pass the time of center of glitch '
                                                                          'if it is not 2 sec before end of segment')
    return vars(parser.parse_args())


def main():
    input_args = setup_parser()
    print("processing data file: {}".format(input_args['raw_file']))
    ofs = preprocessdata(input_args['raw_file'], input_args['outfile'], split_test=input_args["split_test"],
                         srate=input_args['srate'], glitch_seglen=input_args['glitchseglen'],
                         highpass=input_args['highpass'], lowpass=input_args['lowpass'],
                         gps=input_args["glitch_center"])
    print("Saved processed data at: ")
    for f in ofs:
        print(f)


if __name__ == '__main__':
    main()