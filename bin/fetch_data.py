#!/usr/bin/env python

# [jonathan.merritt@ldas-pcdev5 glitchdata]$ source /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/etc/profile.d/conda.sh[jonathan.merritt@ldas-pcdev5 #glitchdata]$ conda activate igwn-py37

"""
This script is meant to run on CIT and grabs blip glitch data of given segment length

#!/usr/bin/env python

"""

import os
import numpy as np
import pandas as pd
from gwpy.timeseries import TimeSeries
from argparse import ArgumentParser

IFOMAP = {
    "H1": {"obs": "H", "type": "H1_HOFT_C02", "channel": "H1:DCS-CALIB_STRAIN_C02"},
    "L1": {"obs": "L", "type": "L1_HOFT_C02", "channel": "L1:DCS-CALIB_STRAIN_C02"},
    "V1": {"obs": "V", "type": "V1Online", "channel": "V1:Hrec_hoft_16384Hz"}
}


def setup_parser():
    parser = ArgumentParser()
    parser.add_argument("blip-file", type=str, help='gspy .csv file with glitch info')
    parser.add_argument("--out-file", type=str, default=None, help='path to save data to (defaults to same name as csv '
                                                                   'file)')
    parser.add_argument("--whiten-seglen", type=float, default=64, help='segment length to save for future whitening '
                                                                        '(seconds)')
    parser.add_argument("--srate", type=float, default=4096., help='sample rate to save data at (Hz)')
    parser.add_argument("--max", type=int, default=None, help="optional max number of glitches to grab")
    parser.add_argument("--extra-t", type=float, default=2., help="time to save after glitch center. glitch center will"
                                                                  " be at white_seglen - extra_t seconds")
    parser.add_argument("--start_idx", type=int, default=0,
                        help="for restarting a pulling run partway, specify a starting glitch")
    return vars(parser.parse_args())


def main():
    input_args = setup_parser()
    blip_dataframe = pd.read_csv(input_args["blip-file"])

    meta_csv = open(os.path.join(os.getcwd(), "meta_{}.csv".format(input_args["out_file"][0:-4])), 'w+',
                    newline='')  # save a csv of metadata along with npy file
    columns = blip_dataframe.columns
    columnsline = ', '.join(columns)
    meta_csv.write('index' + ',' + columnsline + '\n')  #

    blips = None
    glitch_seglen = input_args["whiten_seglen"]
    srate = input_args["srate"]
    center = glitch_seglen - input_args["extra_t"]
    outlength = len(blip_dataframe) if input_args["max"] is None else min([input_args["max"], len(blip_dataframe)])
    start_idx = input_args["start_idx"]
    for i in range(start_idx, outlength):
        print("Fetching blip {}/{}".format(i, outlength))
        try:
            blip_info = blip_dataframe.iloc[i]
        except:
            print("only found {} blips in csv file ending here".format(i - 1))
            break
        ifo = blip_info[11]
        detector = IFOMAP[ifo]["obs"]
        frame_type = IFOMAP[ifo]["type"]
        channel = IFOMAP[ifo]["channel"]
        if "o3" in input_args["blip-file"].lower():
            frame_type = "{}_HOFT_C00".format(ifo)  # frame_type = "{}_HOFT_C00".format(ifo)
            channel = "{}:GDS-CALIB_STRAIN_CLEAN".format(ifo)  # channel = "{}:DCS-CALIB_STRAIN_C01".format(ifo)
        gps = float(blip_info[0])
        data_find_command = "/usr/bin/gw_data_find -o {} -t {} -s {} -e {}".format(
            detector, frame_type, int(gps - glitch_seglen + input_args["extra_t"]), int(gps + input_args["extra_t"])
        )
        print(data_find_command)
        stream = os.popen(data_find_command)
        # print(stream.read())
        filepath = stream.read()[16:-1]
        print("filepath  ={}".format(filepath))
        if "/archive/frames" in filepath:
            print("skipping tape file")
            continue
        try:
            ts = TimeSeries.read(filepath, channel, start=gps - glitch_seglen + input_args["extra_t"],
                                 end=gps + input_args["extra_t"])
            ts = ts.resample(srate)

        except:
            print("file not found skipping")
            continue
        if blips is None:
            blips = np.zeros((outlength, len(ts)))
        blips[i, :] = ts.value
        infolist = blip_info.values.astype(str)
        infoline = ', '.join(infolist)
        meta_csv.write(str(i) + ',' + infoline + '\n')  #

        # write metadata if data was saved.
        if i % 100 == 0:
            try:

                np.save('./tempsave{}.npy'.format(input_args["out_file"][0:-4]), blips)  # change to out file autosaving

                print("quicksaved")
            except:
                print("quicksaving failed")
    of = input_args['out_file']
    # of = "data/{}/".format(ifo.lower()) + of
    # of = "".join(of.split('.')[0].split('/')[1:]) + '{}glitches_{}Hz_{}sec_cent_{}s.npy'.format(outlength, srate, glitch_seglen, center)
    np.save(of, blips)  # , allow_pickle=False)


if __name__ == "__main__":
    main()
