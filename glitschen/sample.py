import numpy as np
import kombine
from scipy.stats.distributions import norm
from glitschen.utils import *


def initializer(nwalkers, timescale, q=5):
    '''initializes p0 for the sampler
    :param q: default 5 ppca components.
    :param nwalkers: required.
    :param timescale: width for initial gaussian on tcent.
    :return:
    '''
    randparams = np.random.normal(loc=0, scale=3, size=(nwalkers, q))
    randtime = np.random.normal(loc=0.0, scale=timescale, size=(nwalkers, 1))
    randparams = np.append(randtime, randparams, axis=1)
    return randparams

def build_reconstructions(W, samps, post):
    '''takes samples with ppca weights and center times, uses W to build timeseries reconstructions.

    :param W: matrix with t.d eigenvectors, dx(q+1), augmented with mean of training data
    :param samps: samples, with tcent first
    :param post: the posterior object from sampling
    :return reconst_td: time domain reconstructions for every sample
    '''
    z = samps[:, 1:].T
    zaug = np.concatenate((z, np.array([np.ones(shape=z.shape[1])])), axis=0)
    rec = np.dot(W, zaug)  # wtd
    reconst = rec.T
    reconst_td = np.zeros(shape=(samps.shape[0], post.data_t_len))  # initialze padding array
    t_shifts = samps[:, 0]

    for i in range(0, len(samps)):  # pad and align reconstructions. fd way
        reconst_td[i] = np.pad(reconst[i], (post.zeros_before, post.zeros_after))
        reconst_fd = np.fft.rfft(reconst_td[i])
        reconst_fd_shifted = fd_timeshift(reconst_fd, post.tfreqs, t_shifts[i], post.srate)
        reconst_td[i] = np.fft.irfft(reconst_fd_shifted)

    return reconst_td

def gen_logls(samps, post):
    logls = np.zeros(shape=samps.shape[0])
    for i in range(0, len(samps)):
        logls[i] = post.logl(reconstruction=post.reconstruct(params=samps[i]))
    return logls

def gen_delta_logls(samps, post):
    logls = np.zeros(shape=samps.shape[0])
    for i in range(0, len(samps)):
        logls[i] = post.log_l_ratio(reconstruction=post.reconstruct(params=samps[i]))
    return logls

def gen_snrs(samps, post):
    SNRs = np.zeros(shape=samps.shape[0])
    for i in range(0, len(samps)):
        SNRs[i] = post.matched_filter_snr(samps[i])
    return SNRs

def dic(logls):
    '''gives the Deviance Information Criterion, calculated from an array of logls from sampling

    :param logls:
    :return DIC:
    '''
    return -2.0 * (np.mean(logls) - np.var(logls))

class Posterior(object):
    def __init__(self, data, W, srate=2048, prior=None, gpr_width=1):
        self.data = data  # fd
        self.data_t = np.fft.irfft(data)
        self.tfreqs = np.linspace(start=0., stop=srate /2, num=len(data))  # freq array of test seg
        # print(len(self.data), len(self.data_t))
        self.data_t_len = len(self.data_t)
        self.W = W  # now TD weights
        self.srate = srate
        if prior is None:
            self.sigma = gpr_width
            self.prior = self._gaussian_prior
        else:
            self.prior = prior
        self.trainlen = W.shape[0] - 1  # td length, W is dxq, with d the stacked FD length.
        #-1 to account for mean packing
        self.zeros_before = int((self.data_t_len - self.trainlen) /2)  # for padding
        self.zeros_after = int((self.data_t_len - self.trainlen) /2)#
        if self.trainlen < len(self.data_t):
            self.vary_time = True
        else:
            self.vary_time = False
    
    def _gaussian_prior(self, x):
        return np.sum(np.log(1/(np.sqrt(2*np.pi)*self.sigma)*np.exp(-0.5*(x/self.sigma)**2)))
    
    def optimal_snr_squared(self, reconstruction):
        return inner_product(reconstruction, reconstruction, self.srate)

    def matched_filter_snr(self, params):
        reconstruction = self.reconstruct(params)
        md = inner_product(reconstruction, self.data, self.srate)
        mm = self.optimal_snr_squared(reconstruction)
        return md /(mm**(0.5))

    def noise_logl(self):
        logl  = -inner_product(self.data, self.data, self.srate ) /2
        return float(np.real(logl))

    def logl(self, reconstruction):
        log_l_ratio = np.real(inner_product(reconstruction, self.data, self.srate)) \
                      - inner_product(reconstruction, reconstruction, self.srate ) /2
        return log_l_ratio + self.noise_logl()

    def log_l_ratio(self, reconstruction):
        log_l_ratio = np.real(inner_product(reconstruction, self.data, self.srate)) \
                      - inner_product(reconstruction, reconstruction, self.srate ) /2
        return log_l_ratio

    def logprior(self, params):
        if self.vary_time:
            t_shift = params[0]
            weights = params[1:]
            tlen = self.data_t_len / self.srate
            if (t_shift < -tlen/2) or (t_shift > tlen/2):
                return -np.inf
            else:
                return self.prior(weights)
        return self.prior(params)

    def reconstruct(self, params):
        if self.vary_time:
            t_shift = params[0]
            weights = params[1:]
            zaug = np.append(weights, 1)
            reconst_td = np.dot(self.W, zaug)  # Get TD reconstrution

            # padding
            reconst_td = np.pad(reconst_td, (self.zeros_before, self.zeros_after))
            # zero pad the reconstruction to be length of data
            # print(reconst_td.shape, self.data_t.shape)
            assert (len(reconst_td) == len(self.data_t))  # assert that reconstruction is the same length as data
            reconst_fd = np.fft.rfft(reconst_td)  # convert back to FD

            # shifting
            reconst_fd_shifted = fd_timeshift(reconst_fd, self.tfreqs, t_shift, self.srate)
            return reconst_fd_shifted
        else:
            recon_td = np.dot(self.W, params)
            reconst_fd = np.fft.rfft(recon_td)
        return reconst_fd

    def __call__(self, params):
        lp = self.logprior(params)
        if np.any(np.isinf(lp)):
            return -np.inf
        recon_fd = self.reconstruct(params)
        return self.logl(recon_fd) + lp


