"""
This file is to store functions to preprocess data for training the PCA
"""
from gwpy.timeseries import TimeSeries
import numpy as np
import tqdm, os
from sklearn.model_selection import train_test_split


def process_single_glitch(timeseries, gps=None, seglen=1, srate=16384., highpass=None, lowpass=None, maxwindow=.2):
    """
    This function processes a single row from our raw .npy blip glitch matrix

    :param timeseries: input row from np matrix (array_like)
    :param gps: optional pass a given time to use as center of glitch (not a GPS time but seconds of segment)
    :param seglen: length of processed segments to save (defaults to 1 second)
    :param srate: srate of the raw data we are processing (all data gets down sampled to 2kHz)
    :param highpass: Optional freq to highpass from (defaults to no Highpassing)
    :param lowpass: Optional freq to lowpass from (defaults to no lowpassing)
    :param maxwindow: Window size to check for re-centering procedure (seconds, defaults to 0.2)
    :return: returns the numpy array of processed time series at 2kHz and of length=seglen in seconds
    """
    times = np.arange(len(timeseries)) / srate
    ts = TimeSeries(timeseries, times=times)
    if gps is None:
        duration = float(len(timeseries)) / srate
        gps = duration-2.  # gspy center time
    if highpass is not None:
        ts = ts.highpass(highpass)  # highpass at given value
    if lowpass is not None:
        ts = ts.lowpass(lowpass)
    ts = ts.whiten()
    sel = (times<gps+maxwindow/2.) & (times>gps-maxwindow/2.)  # .2s window by default
    exarg = np.argmax(np.abs(ts[sel]))  # local maximum
    gps = times[sel][exarg]
    ts = ts.crop(gps - seglen / 2.0, gps + seglen / 2.0)
    if srate > 2048.:
        ts = ts.resample(2048.)
    return ts.value


def preprocessdata(raw_data_file, outfile, split_test=False, srate=16384, glitch_seglen=1, **kwargs):
    """
    Function to process an entire file of blip data

    :param raw_data_file: .npy file with each row the unprocessed blip timeseries
    :param outfile: name of the file to save processed data to
    :param split_test: Boolean flag to split 10% of glitches out for testing (defaults to False) (if used it appends
        _test or _train to the outfile and saves both files
    :param srate: input sample rate of raw data
    :param glitch_seglen: the segment length of glitch data we want to save for training/testing purposes
        (default to 1 sec)
    :param kwargs: all key-word args get passed to process_single_glitch (these include filtering args, etc)
    :return: returns the the paths to each file that was written and saved (a single one if not splitting test and two
        if split_test = True)
    """
    raw_timeseries_data = np.load(raw_data_file)
    # raw_timeseries data should have shape (nblips, tlen), tlen length of each blip timeseries
    nblips = raw_timeseries_data.shape[0]
    processed_data = np.empty((nblips, int(glitch_seglen*srate))) # initialize matrix
    glitch_idxs = [] # save which indexes have glitch
    for i in tqdm.tqdm(range(nblips)): # setup progress bar
        if np.count_nonzero(raw_timeseries_data[i,:]) == 0: # if all zeros means we didnt save a glitch so go to next
            continue
        glitch_idxs.append(i) # if we have a glitch save the indez
        try: # try processing the data and if it doesnt fit from one off rounding error try with last value cut off
            processed_data[i, :] = process_single_glitch(raw_timeseries_data[i, :], seglen=glitch_seglen, srate=srate,
                                                         **kwargs)
        except:
            processed_data[i, :] = process_single_glitch(raw_timeseries_data[i, :], seglen=glitch_seglen, srate=srate,
                                                         **kwargs)[:-1]
    processed_data = np.take(processed_data, glitch_idxs, axis=0) # only keep the rows with glitches
    outfiles = [outfile]
    if split_test: # if we split then reformat the outfile naming
        training_data, testing_data = train_test_split(processed_data, test_size=0.1, shuffle=True)
        testfn = outfile.split('.npy')[0]+'{}Hz_{}s_test'.format(srate, glitch_seglen)+outfile.split('.npy')[-1]
        trainfn = outfile.split('.npy')[0]+'{}Hz_{}s_train'.format(srate, glitch_seglen)+outfile.split('.npy')[-1]
        outfiles = [testfn, trainfn]

    for outf in outfiles: # make dirs if not there and save the testing and training data
        dir = "/".join(outf.split("/")[:-1])
        if not os.path.exists(dir):
            os.mkdir(dir)
        if split_test:
            if "train" in outf:
                np.save(outf, training_data)
            elif "test" in outf:
                np.save(outf, testing_data)
        else:
            np.save(outf, processed_data)
    return outfiles

