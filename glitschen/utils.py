import numpy as np
import pickle
from scipy.signal import tukey
import matplotlib.pyplot as plt
from scipy.stats.distributions import norm
from matplotlib import gridspec
import matplotlib.patches as mpatches
from corner import corner
from sklearn.model_selection import train_test_split
import kombine

def inner_product(aa, bb, srate):
    '''Inner product of two frequencyseries vectors
    :param aa:
    :param bb:
    :param srate:
    :return:
    '''
    factor = np.sqrt(2 * srate)  # gwpy whitening factor
    frequency = np.linspace(start=0., stop=srate / 2, num=len(aa))  # list of frequencies
    integrand = np.conj(aa / factor) * bb / factor
    # integrand = np.conj(aa) * bb
    df = frequency[1] - frequency[0]
    integral = np.sum(integrand) * df  # df is 1/duration
    return 4. * np.real(integral)

def tukey_window(size, alpha=1.0/8.0):
    """Returns a normalized Tukey window function that tapers the first
    ``alpha*size/2`` and last ``alpha*size/2`` samples from a stretch of
    data.
    """

    istart = int(round(alpha*(size-1)/2.0 + 1))
    iend = int(round((size-1)*(1-alpha/2.0)))

    window = np.ones(size)
    ns = np.arange(0, size)

    window[:istart] = 0.5*(1.0 + np.cos(np.pi*(2.0*ns[:istart]/(alpha*(size-1))-1)))
    window[iend:] = 0.5*(1.0 + np.cos(np.pi*(2.0*ns[iend:]/(alpha*(size-1)) - 2.0/alpha + 1.0)))

    wnorm = np.sqrt(np.sum(np.square(window))/size)

    return window/wnorm

def fd_timeshift(h, freqs, time_shift, sample_rate):
    """Frequency Domain time shifting (cyclic)

    Parameters
    ----------
    """
    N = len(h)
    dt = float(time_shift)

    shift = np.array(np.exp(-2j * np.pi * dt * freqs))
    h *= shift

    return h


def tfrac_to_windowtime(tfrac, srate, trainlen, testlen):
    """Maps tfrac shift [0,1] onto gps time from center"""
    window = (testlen - trainlen) / srate
    fshift = tfrac - 0.5
    return window * fshift


def save_result_to_file(result, filename):
    with open(filename, "wb") as f:
        pickle.dump(result, f)
    return filename


def maxL_reconst(Waug, test, srate=2048, fixed=True, scoreplots=False, gfrac = 0.25,\
                 metric='logl'):
    '''analytically calculates maxL on a timegrid, does not perform subsample interpolation.
    :param Waug: array of training eigenvectors, dim dx(q+1), augmented with the training mean as the last column.
    :param test: test segment, expects longer or equal to training length
    :param fixed: defaults to fixed, if false will timeslide and return maxL with alignment
    :return maxL WEIGHTS
    '''
    W = Waug[:, :-1]
    noise_std = 1.  # fixed, as in training script.
    M = np.matmul(W.T, W) + noise_std ** 2 * np.identity(W.shape[1])
    trainlen = W.shape[0]
    testlen = len(test)
    Winv = np.linalg.pinv(W)
    Minv = np.linalg.pinv(M)
    mu = Waug[:, -1]  ##is this the way??

    if fixed == True:  # center fixed
        # crop the test seg
        tcent = int(testlen / 2)
        tc = test[int(tcent - trainlen / 2):int(tcent + trainlen / 2)] - mu  # [:,None]
        z = np.matmul(Minv, np.dot(W.T, tc))  # the maxl training weights for this particular reconstruction
        z = np.append(z, 1)
        rec = np.dot(z.T, Waug.T)

        zbefore = int((testlen - trainlen) / 2)
        zafter = int((testlen - trainlen) / 2)
        recpad = np.pad(rec, (zbefore, zafter))  # padded to match test seg length
        return recpad

    else:  # timeslides
        sigcentidx = int(testlen / 2)
        idx_win = trainlen
        window = tukey(idx_win, .2)
        tidx = np.arange(0, int((len(test) - idx_win)), 1)
        leeway = len(tidx)
        tslides = np.zeros(shape=(int((len(test) - idx_win)), idx_win))
        for i in range(0, len(tidx)):
            tslides[i] = test[int(tidx[i]):int(tidx[i] + idx_win)] - mu
        scores = np.zeros(len(tidx))

        z = np.matmul(Minv, np.dot(W.T, tslides.T))

        zaug = np.concatenate((z, np.array([np.ones(shape=z.shape[1])])), axis=0)
        recs = np.dot(zaug.T, Waug.T)

        test_fds = np.fft.rfft(window * tslides, axis=1)
        recon_fds = np.fft.rfft(window * recs, axis=1)

        if metric == 'logl':
            for i in range(0, len(scores)):
                scores[i] = rec_logl(recon_fds[i], test_fds[i], srate)
        else: # 'inner _product'
            for i in range(0, len(scores)):
                scores[i] = inner_product(recon_fds[i], test_fds[i], srate)

        g1 = 0.5 - gfrac/2. #gateing fraction
        g2 = 0.5 + gfrac/2.
        mxidx = np.argmax(scores[int(leeway * g1):int(leeway * g2)]) + int(leeway * g1)
        #print('max index/leeway', mxidx/leeway)
        if scoreplots == True:
            plt.figure()
            plt.plot(scores)

        zbefore = mxidx
        zafter = testlen - trainlen - mxidx
        recpad = np.pad(recs[mxidx], (zbefore, zafter))  # padded to match test seg length
        return recpad


def maxL_reconst_logl(Waug, test, srate=2048, fixed=True, scoreplots=False,\
                 metric='logl'):
    '''Now the same as maxL_reconst, will probably remove soon.

    analytically calculates maxL on a timegrid, does not perform subsample interpolation.
    :param Waug: array of training eigenvectors, dim dxq+1, with the training mean as the last column.
    :param test: test segment, expects longer or equal to training length
    :param fixed: defaults to fixed, if false will timeslide and return maxL with alignment (logl based in this version)
    :return maxL WEIGHTS
    '''

    return maxL_reconst(Waug, test, srate=srate, fixed=fixed, scoreplots=scoreplots, metric=metric)


def maxL_tcent(Waug, test, srate=2048, scoreplots=False, gfrac=0.25,\
                 metric='logl'):
    '''returns center time that maximizes Logl.
    :param Waug: array of training eigenvectors, dim dx(q+1), augmented with the training mean as the last column.
    :param test: test segment, expects longer or equal to training length
    :param fixed: defaults to fixed, if false will timeslide and return maxL with alignment
    :return maxL time
    '''
    tlen = len(test) / srate
    times = np.linspace(-tlen / 2, tlen / 2, num=int(tlen * srate))

    W = Waug[:, :-1]
    noise_std = 1.  # fixed, as in training script.
    M = np.matmul(W.T, W) + noise_std ** 2 * np.identity(W.shape[1])
    trainlen = W.shape[0]
    testlen = len(test)
    Winv = np.linalg.pinv(W)
    Minv = np.linalg.pinv(M)
    mu = Waug[:, -1]

    sigcentidx = int(testlen / 2)
    idx_win = trainlen
    window = tukey(idx_win, .2)
    tidx = np.arange(0, int((len(test) - idx_win)), 1)
    leeway = len(tidx)
    tslides = np.zeros(shape=(int((len(test) - idx_win)), idx_win))
    for i in range(0, len(tidx)):
        tslides[i] = test[int(tidx[i]):int(tidx[i] + idx_win)] - mu
    scores = np.zeros(len(tidx))

    z = np.matmul(Minv, np.dot(W.T, tslides.T))

    zaug = np.concatenate((z, np.array([np.ones(shape=z.shape[1])])), axis=0)
    recs = np.dot(zaug.T, Waug.T)

    test_fds = np.fft.rfft(window * tslides, axis=1)
    recon_fds = np.fft.rfft(window * recs, axis=1)

    if metric == 'logl':
        for i in range(0, len(scores)):
            scores[i] = rec_logl(recon_fds[i], test_fds[i], srate)
    else:  # 'inner _product'
        for i in range(0, len(scores)):
            scores[i] = inner_product(recon_fds[i], test_fds[i], srate)
    g1 = 0.5 - gfrac/2. #gateing fraction
    g2 = 0.5 + gfrac/2.
    if scoreplots == True:
        plt.figure()
        plt.plot(scores)


    mxidx = np.argmax(scores[int(leeway * g1):int(leeway * g2)]) + int(leeway * g1)
    tcent = times[int(len(times) / 2 - leeway / 2 + mxidx)]
    return tcent


def maxL_weights(Waug, test, srate=2048, fixed=True, scoreplots=False, gfrac=0.25,\
                 metric='logl'):
    '''analytically calculates maxL on a timegrid, does not perform subsample interpolation.
    :param Waug: array of training eigenvectors, dim dx(q+1), augmented with the training mean as the last column.
    :param test: test segment, expects longer or equat to training length
    :param fixed: defaults to fixed, if false will timeslide and return maxL with alignment
    :return maxL WEIGHTS
    '''
    W = Waug[:, :-1]
    noise_std = 1.  # fixed, as in training script.
    M = np.matmul(W.T, W) + noise_std ** 2 * np.identity(W.shape[1])
    trainlen = W.shape[0]
    testlen = len(test)
    Winv = np.linalg.pinv(W)
    Minv = np.linalg.pinv(M)
    mu = Waug[:, -1]

    if fixed == True:  # center fixed
        # crop the test seg
        tcent = int(testlen / 2)
        tc = test[int(tcent - trainlen / 2):int(tcent + trainlen / 2)] - mu  # [:,None]
        z = np.matmul(Minv, np.dot(W.T, tc))  # the maxl training weights for this particular reconstruction
        return z

    else:  # timeslides
        sigcentidx = int(testlen / 2)
        idx_win = trainlen
        window = tukey(idx_win, .2)
        tidx = np.arange(0, int((len(test) - idx_win)), 1)
        leeway = len(tidx)
        tslides = np.zeros(shape=(int((len(test) - idx_win)), idx_win))
        for i in range(0, len(tidx)):
            tslides[i] = test[int(tidx[i]):int(tidx[i] + idx_win)] - mu
        scores = np.zeros(len(tidx))

        z = np.matmul(Minv, np.dot(W.T, tslides.T))

        zaug = np.concatenate((z, np.array([np.ones(shape=z.shape[1])])), axis=0)
        recs = np.dot(zaug.T, Waug.T)

        test_fds = np.fft.rfft(window * tslides, axis=1)
        recon_fds = np.fft.rfft(window * recs, axis=1)

        if metric == 'logl':
            for i in range(0, len(scores)):
                scores[i] = rec_logl(recon_fds[i], test_fds[i], srate)
        else: # 'inner _product'
            for i in range(0, len(scores)):
                scores[i] = inner_product(recon_fds[i], test_fds[i], srate)

        g1 = 0.5 - gfrac / 2.  # gateing fraction
        g2 = 0.5 + gfrac / 2.
        if scoreplots == True:
            plt.figure()
            plt.plot(scores)

        mxidx = np.argmax(scores[int(leeway * g1):int(leeway * g2)]) + int(leeway * g1)

        return z.T[mxidx]


def opt_snr_squared(rec, srate=2048):
    '''Calculates the optimal SNR squared of a time domain vector.

    :param rec: Time domain signal vector to reconstruct
    :param srate: defaults to 2048
    :return: Optimal SNR squared
    '''
    recfd = np.fft.rfft(rec)
    return inner_product(recfd, recfd, srate)


def mf_snr(data, recon, srate=2048):
    '''Calculates the Matched Filter SNR of two time domain vectors

    :param data: run segment, actual data in time domain
    :param recon: reconstruction/signal segment, TD data, must be same length.
    :param srate: default 2048
    :return: Matched Filter SNR
    '''
    reconfd = np.fft.rfft(recon)
    datafd = np.fft.rfft(data)
    md = inner_product(reconfd, datafd, srate)
    mm = opt_snr_squared(recon)
    return (md / (np.sqrt(mm)))


def rec_logl(rec, test, srate=2048):
    nlogl = -inner_product(test, test, srate) / 2
    noise_logl = float(np.real(nlogl))
    log_l_ratio = np.real(inner_product(rec, test, srate)) \
                  - inner_product(rec, rec, srate) / 2
    logl = log_l_ratio + noise_logl
    return logl

def delta_logl(rec, test, srate=2048):
    log_l_ratio = np.real(inner_product(rec, test, srate)) \
                  - inner_product(rec, rec, srate) / 2
    return log_l_ratio

def rec_logls(recs, tests, srate=2048):
    logls = np.zeros(shape=recs.shape[0])
    for i in range(0, len(recs)):
        logls[i] = rec_logl(recs[i], tests[i], srate)
    return logls

def rec_delta_logls(recs, tests, srate=2048):
    logls = np.zeros(shape=recs.shape[0])
    for i in range(0, len(recs)):
        logls[i] = delta_logl(recs[i], tests[i], srate)
    return logls

def plot_residuals(rec, test, outdir, fn, title, bins=200, ylim=1e-5):

    clean = test - rec
    plt.figure(figsize=(16, 4.75))
    _, bins, _ = plt.hist(test.flatten(), density=True, bins=bins, alpha=0.9, label='Whitened Data', color='blue')
    plt.hist(clean.flatten(), bins=bins, density=True, label='Whitened Residuals', alpha=0.9, color='orange')

    plt.plot(bins, norm.pdf(bins), label=r'$\mathcal{N}(\mu=0, \sigma^2=1)$', linewidth=2.0, color='k', ls = '--')
    plt.gca().set_yscale('log')
    plt.ylabel(r'$p(\tilde{d}_i)$')
    plt.xlabel(r'$\tilde{d}_i$ (whitened amplitude)')
    plt.legend()
    plt.ylim(ylim, 1)
    plt.title('{}: test set residuals after glitch subtraction'.format(title))
    # plt.xlim(-45, 45)
    plt.savefig(outdir + fn)

def plot_sampler_result(W, test, reconst_td, samps, title=None, srate=2048, bins=500, xlim1=-0.25, xlim2=0.25, \
                        gfrac=0.25, metric='logl'):
    tlen = len(test) / srate
    times = np.linspace(-tlen / 2, tlen / 2, num=int(tlen * srate))

    fig = plt.figure(figsize=(13, 6), dpi=400)
    gs = gridspec.GridSpec(2, 1, height_ratios=[3, 1])

    ax0 = plt.subplot(gs[0])
    ax0.plot(times, test, label='data', color = 'blue')
    ax0.plot(times, reconst_td.T, alpha=0.05, color='orange')
    ax0.plot(times, maxL_reconst(W, test, fixed=False, gfrac=gfrac, metric=metric), color='springgreen', label='max$\mathcal{L}$ reconstruction');
    ax0.set_ylabel('whitened amplitude')
    patch = mpatches.Patch(color='orange', label='Posterior Samples')
    handles, labels = ax0.get_legend_handles_labels()
    handles.append(patch)
    leg = ax0.legend(handles=handles, loc='best')

    ax1 = plt.subplot(gs[1], sharex=ax0)
    ax1.hist(samps[:, 0], bins=bins, color='orange', range=(xlim1, xlim2))
    ax1.set_ylabel('center time samples')
    ax1.axvline(maxL_tcent(W, test, metric=metric, gfrac=gfrac), color='springgreen')
    plt.setp(ax0.get_xticklabels(), visible=False)
    # remove last tick label for the second subplot
    yticks = ax1.yaxis.get_major_ticks()
    yticks[-1].label1.set_visible(False)
    plt.xlim(xlim1, xlim2)
    plt.xlabel('time from center [s]')
    plt.subplots_adjust(hspace=.2)
    plt.title(title)

def plot_eigfuncs_stacked(W, title, outdir=None, xlim=.25, scale=0.2, srate=2048, save=False):
    tlen = W.shape[0] / srate
    times = np.linspace(-tlen / 2, tlen / 2, num=int(tlen * srate))
    plt.figure(figsize=(8, 12))
    plt.plot(times, 1 + np.arange(W.shape[1]) + scale * W)
    plt.xlabel('time [s]')
    plt.ylabel('principal axes')
    plt.title(title)
    plt.xlim(-xlim, xlim)
    plt.axvline(x=0, ls='--', color='k', alpha=0.4)

    if save == True:
        plt.savefig(outdir, dpi=600);


def plot_3d_scatter():
    fig = plt.figure(1, figsize=(10, 10))
    ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
    ax.scatter(Ztrain.T[:, 0], Ztrain.T[:, 1], Ztrain.T[:, 2], cmap=plt.cm.nipy_spectral,
               edgecolor='k', label='training data', alpha=0.3)
    ax.legend()
    # fig.savefig('../result/plots/training/{}_3spaceweights.png'.format(outtag))


def plot_corner_train():
    fig = plt.figure()
    fig = corner(Ztrain.T, color='orange');
    # fig.savefig('../result/plots/training/{}_corner.png'.format(outtag))


def plot_corner_sample(Z, thinsamps, W, test, t_ylim=800, bins=30, gfrac=0.25, metric='logl'):
    ndim = thinsamps.shape[1]
    zplot = np.concatenate((np.array([np.ones(Z.shape[1])]) * np.mean(thinsamps[:, 0]), Z[:-1]), axis=0)
    truths = np.append(maxL_tcent(W, test, gfrac=gfrac, metric=metric), maxL_weights(W, test, fixed=False,\
                                                gfrac=gfrac, metric=metric))
    frac = 1.
    # pltrange = [(np.min(thinsamps[:,0]), np.max(thinsamps[:,0]))]
    pltrange = [1]
    labels = ['$t_{center}$']
    for i in range(0, Z.shape[0] - 1):
        pltrange.append(frac)
        labels.append('$z_{}$'.format(i + 1))
    figure = corner(thinsamps, labels=labels, color='orange', truths=truths, \
                    truth_color='springgreen', range=pltrange, scale_hist=True, bins=bins);
    corner(zplot.T, color='blue', alpha=0.5, range=pltrange, \
           fig=figure, scale_hist=True, bins=bins);

    plt.legend(['MaxL reconst', 'posterior samples', 'training glitches'], bbox_to_anchor=(0, 6))

    column_to_remove = 0
    axes = np.array(figure.axes).reshape((ndim, ndim))
    # remove second (idx 1) line from marginal axes
    axes[0, column_to_remove].get_children()[1].set_alpha(0)

    for ii in range(1, ndim):
        children = axes[ii, column_to_remove].get_children()
        for child in children[6:13]:
            child.set_alpha(0)
        children[16].set_alpha(0)

    axes[0, 0].set_ylim(0, t_ylim);  # axes[1,1].get_ylim())

def plot_corner_maxl(Z, W, test, t_ylim=800, bins=30):
    zplot = Z[:-1]
    truths = maxL_weights(W, test, fixed=False, gfrac=0.1, metric='ip')
    corner(zplot.T, color='blue', alpha=0.5, truths=truths,\
        scale_hist=True, bins=bins, truth_color='springgreen');
