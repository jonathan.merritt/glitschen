from glitschen import *
import numpy as np
import matplotlib.pyplot as plt
from gwpy.timeseries import TimeSeries
from corner import corner
from tqdm import tqdm
from scipy.signal import tukey
from scipy.stats.distributions import norm
from mpl_toolkits.mplot3d import Axes3D
from sklearn.model_selection import train_test_split
import kombine


def train_ppca(glitches_fn, plots=True, outtag=None, srate = 2048, winlen = 0.5, q = 5):
    """Trains the PPCA model on a population of glitches, in a .npy array of timeseries vectors.

    :param glitches_fn: the path to the glitch file
    :param plots: whether to generate plots from training
    :param outtag: a name for the trained model, otherwises uses the glitch file name
    :param winlen: the desired training length, in seconds. default 0.5s, adequate for blips and tomtes.
    :param srate: the sample rate of the data, Hz
    :param q: the # of components to train with, default 5
    :return W: The matrix containing trained eigenvectors
    :return Z: The matrix containing maxL training weights for all the training glitches, to be used in kde prior.
    """
    if outtag == None:
        outtag = glitches_fn[:-4]

    glitches = np.load(glitches_fn)
    times = np.arange(glitches.shape[1]) / srate
    siglen = glitches.shape[1]/srate

    train_size = glitches.shape[0]
    timeseries_data = []

    white_timeseries_data = np.empty((train_size, int(srate * siglen)))

    for i, raw_ts in enumerate(glitches):
        ts = TimeSeries(raw_ts, times=times)
        white_timeseries_data[i] = ts.value
        #white_freqseries_data[i] = ts.fft().value
        timeseries_data.append(ts)


    sigcentidx = int(glitches.shape[1]/2)  # where to center our glitches. assumes centered
    #found a potential error in sigcentidx
    idx_win = int(winlen*srate)  # segment length we want to train with in samples
    window = tukey(idx_win, .2)
    sig_start_idx = sigcentidx - idx_win // 2
    sig_end_idx = sigcentidx + idx_win // 2


    X_fd = np.fft.rfft(window * white_timeseries_data[:, sig_start_idx:sig_end_idx])
    X = np.concatenate([X_fd.real, X_fd.imag], axis=1).T  # packed F domain

    mu = X.mean(axis=1) #mean in stacked f domain
    mu_re, mu_im = np.split(mu, 2)
    mu_fd = mu_re + 1j*mu_im
    mu_td = np.fft.irfft(mu_fd) #time domain mean of training data

    X -= mu[:, np.newaxis]  #need to subtract the mean
    noise_std = 1 / 2 / (idx_win / srate)  # was 2/(idx_win/srate), led to \simga = 2.0

    #train the model
    num_datapoints = X.shape[1]  # = N
    data_dim = X.shape[0]  # =d
    stddv_datapoints = noise_std

    S = np.cov(X)  # sample covariance of observations
    λ, U = np.linalg.eigh(S)  # eigenvalue decomposition of S

    λ = (λ[::-1])  # eigenvalues
    U = (U[:, ::-1])  # eigenvectors (we get them all for free)
    λs = λ[:q]  # truncate eigenvalues (take q)
    Uq = U[:, :q]
    Λq = np.diagflat(λs)  # qxq

    # W is dxq
    W = np.matmul(Uq, np.sqrt(Λq - np.diagflat(noise_std ** 2 * np.ones(q))))  # let R=I
    #Waugt = np.concatenate((W.T, np.array([mu])), axis=0) #packing the mean
    #Waug = Waug.T
    #by default, the last row of W will be the mean, to store fewer objects. (augmented)
    #will pack the mean in time domain instead.

    #C = np.matmul(W, W.T) + noise_std ** 2 * np.identity(W.shape[0])  # covariance
    M = np.matmul(W.T, W) + noise_std ** 2 * np.identity(W.shape[1])

    # reconstruction steps
    Minv = np.linalg.pinv(M)  # pseudoinverse, since w is not square.

    Ztrain = np.matmul(Minv, np.dot(W.T, X))
    Ztrainaug = np.concatenate((Ztrain, np.array([np.ones(shape=Ztrain.shape[1])])), axis=0)
    #augment Z with a q+1th dimension, of ones, factoring out mean addition. 

    # if input_args["test"]==True:
    #     Ztest = np.matmul(Minv, np.dot(W.T, testX)) #- mu[:, np.newaxis]))
    #     rec = np.dot(W, Ztest)  # all test set reconstructions, in packed F-domain

    w_fd_re, w_fd_im = np.split(W, 2, axis=0)
    w_fd = w_fd_re + 1j * w_fd_im
    Wtd = np.fft.irfft(w_fd, axis=0)
    Waugtdt = np.concatenate((Wtd.T, np.array([mu_td])), axis=0) #packing the mean
    Waugtd = Waugtdt.T
    # by default, the last row of W will be the mean, to store fewer objects. (augmented)

    #np.save('../result/models/trained/W_{}.npy'.format(outtag), Waugtd) #save the time domain training eigenvectors.
    #np.save('../result/models/trained/Z_{}.npy'.format(outtag), Ztrainaug) #save the training weights.
    #fix outdirectories.

    #if plots==True:
        #plot_training()

    return Waugtd, Ztrainaug

#def test_model(test_set, model, plots=True):
#should generate reconstructions, residuals, plots, etc for a testset
def generate_test_set_residuals(train_fn, winlen, test_fn, outdir, tag, title, q=5, fixed=True, ylim=1e-5,\
                                srate=2048, lowpass=10, bins=300, gfrac=0.25, metric='logl'):
    test = np.load(test_fn)
    W, Z = train_ppca(train_fn, outtag=tag, q=q, winlen=winlen)

    rec = np.zeros(shape=(test.shape))
    # plt.figure(figsize=(10, 2))
    for i in range(0, len(test)):
        reconst = maxL_reconst(W, test[i], fixed=fixed, gfrac=gfrac, metric=metric)
        rec[i] = reconst

    freqstest = np.fft.rfftfreq(test.shape[1], 1 / srate)  # pick passband freqs
    freqsrec = np.fft.rfftfreq(rec.shape[1], 1 / srate)
    print(freqstest, freqsrec)
    testfd = np.fft.rfft(test)[:, freqstest > lowpass]
    recfd = np.fft.rfft(rec)[:, freqsrec > lowpass]
    testcomb = np.concatenate((np.imag(testfd), np.real(testfd)))
    reccomb = np.concatenate((np.imag(recfd), np.real(recfd)))
    # print(testcomb)
    # print(testcomb.shape)
    scipy_tukey = tukey(test.shape[1], .2)
    wnorm = np.sqrt(np.sum(np.square(scipy_tukey)) / test.shape[1])
    # account for integrated losses from tukey windowing.
    fac = np.sqrt(test.shape[1] / 2) / wnorm  # scaling for correct FD width.

    plot_residuals(reccomb / fac, testcomb / fac, outdir, tag + '_residuals', title, ylim=ylim, bins=bins)

def outlier_score(glitches_fn, test_seg, outtag=None, srate = 2048, winlen = 0.5, q = 5, plots=True,  \
                  fixedglitches=True, fixedtestseg=True, gfrac=0.1, metric='ip'):
    glitches = np.load(glitches_fn)
    train, test = train_test_split(glitches, train_size=0.5, shuffle=True)
    train_path = glitches_fn.split('.npy')[-1]+'outliertest'+'.npy'
    test_path = glitches_fn.split('.npy')[-1]+'outliertrain'+'.npy'
    np.save(train_path, train)
    np.save(test_path, test)

    W, Z = train_ppca(train_path, q=q, plots=True, winlen=winlen)
    #Wtest, Ztest = train_ppca(test_path, q=q, plots=True)

    truths = maxL_weights(W, test_seg, fixed=fixedtestseg, gfrac=gfrac, metric=metric)
    kdeprior = kombine.clustered_kde.optimized_kde(Z[:-1].T)

    testweights = np.copy(Z)
    for i in range(0, len(testweights)):
        #truth = maxL_weights(Wtest, test_seg, fixed=True)
        truth = maxL_weights(W, test_seg, fixed=fixedglitches)
        testweights[:-1, i] = truth

    logpdfs = kdeprior.logpdf(testweights[:-1].T)
    testseglpdf = kdeprior.logpdf(truths)

    if plots == True:
        plt.figure(figsize=(10, 3))
        plt.hist(logpdfs, bins=100, label='training glitches')
        plt.axvline(kdeprior.logpdf(truths), color='red', label='event')
        plt.xlabel('logpdf')
        plt.legend()

    outlier_score = np.count_nonzero(logpdfs < testseglpdf) / len(logpdfs)
    print('outlier_score: ', outlier_score)
    return outlier_score