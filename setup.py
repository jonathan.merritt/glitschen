from distutils.core import setup


__version__ = "0.0.3"

setup(
    name="glitschen",
    version=__version__,
    url="https://git",
    packages=["glitschen"],
    license="",
    scripts=["bin/fetch_data.py", "bin/process_data.py", "bin/sample_kombine.py", "bin/sample_emcee.py", "bin/train_model.py"],
    long_description=open("README.md").read(),
)
